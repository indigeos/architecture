# Architecture of IndigeOS
IndigeOS is an indigeously-led project for device and server management. It creates an outcomes-based architecture for operating systems by applying best practices for device, server, and application management. The goal is to leverage and promote existing, open source architectures under the following principles common within indigenous communities. As we build this environment, we tell the story of values, endurance, and hard work that typifies the best of indigenous communities worldwide. We seek your help and your stories in this effort.

First, is the absolute distinction between the common(noa) and the sacred(tapu). That which is tapu within our own family and person can only be kept intrinsically sacred by using open and common tools and technologies. To protect that which is tapu using technologies that are owned by others is offensive. Only that which is most noa is acceptable as the unencumbered gift and tool pure enough for that which is sacred. Indigeos is for the world. It's worldly copyright protection is held in trust by the Hei Whiringa Charitable Trust which is governed by indigenous stewards who dedicate these technologies to the world.

To this we apply principles of the fourth way as a means to achieve inner and community sovereignty in spite of oppresive structures.

* STARTING FROM WITHIN
    * Human Beings Can Transform Their Worlds
    * Development Comes From Within
    * No Vision, No Development
    * Healing Is A Necessary Part Of Development
* WORKING IN A CIRCLE
    * Interconnectedness
    * No Unity, No Development
    * No Participation, No Development
    * Justice
* IN A SACRED MANNER
    * Spirit
    * Morals and Ethics
    * The Hurt of One Is the Hurt of All: The Honor of One Is the Honor Of All
    * Authentic Development Is Culturally Based
* WE HEAL AND DEVELOP OURSELVES, OUR RELATIONSHIPS AND OUR WORLD
    * Learning
    * Sustainability
    * Move to the Positive
    * Be the Change You Want To See

# Technical Structures
IndigeOS is an agile operating platform that relies on the open source world around it to cultivate an ecosystem of interworking processes, service, and connections. 

